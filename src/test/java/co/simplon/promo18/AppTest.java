package co.simplon.promo18;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
  
    @Test
    public void shouldAddTwoValues()
    {
        int result = 2+2;

        assertEquals(4, result);
    }
}
