package co.simplon.promo18;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

public class CalculatorTest {
    @Test
    void shouldAddTwoNumbers() {
        Calculator calculator = new Calculator();

        assertEquals(4, calculator.add(2, 2));
        assertEquals(4, calculator.add(3, 1));
        assertEquals(106, calculator.add(6, 100));
        assertEquals(1, calculator.add(-1, 2));

    }

    @Test
    void shouldDivideTwoNumbers() {
        Calculator calculator = new Calculator();

        assertEquals(1, calculator.divide(2, 2));

    }


    @Test
    void shouldThrowExcpetionOnDivideByZero() {
        Calculator calculator = new Calculator();

        assertThrows(ArithmeticException.class, () -> calculator.divide(2,0));

    }
}
