package co.simplon.promo18.todo;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class TaskTest {
    @Test
    void testCheck() {
        Task task = new Task("Label");

        task.check();
       
        assertTrue(task.isDone());

        //assertEquals("☒ Label", task.display());
        


    }

    @Test
    void testDisplay() {
        Task task = new Task("Label");

        assertEquals("☐ Label", task.display());
    }
}
