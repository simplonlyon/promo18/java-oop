package co.simplon.promo18.todo;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe qui contiendra la liste des choses à faire (Tasks)
 * et qui permettra de gérer la liste en question 
 * (rajouter, cocher, vider la liste, etc.)
 */
public class TodoList {
    private List<Task> tasks = new ArrayList<>();

    /**
     * Méthode qui permet de créer une nouvelle Task et de l'ajouter à la liste
     * @param taskLabel Le label de la nouvelle Task
     */
    public void addTask(String taskLabel) {
        Task createdTask = new Task(taskLabel);
        tasks.add(createdTask);
    }
    
    /**
     * Méthode qui va boucler sur toutes les Task contenues dans la TodoList afin
     * de les afficher
     */
    public void displayList() {
        //Boucle foreach qui va itérer sur tous les éléments contenus dans la liste tasks définie ligne 12
        for (Task task : tasks) {
            //Pour chaque Task, on déclenche la méthode display et on affiche son retour
            System.out.println(task.display());
        }
    }


}
