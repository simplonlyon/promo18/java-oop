package co.simplon.promo18.todo;

import java.util.Scanner;

public class CliTodoList {
    private Scanner scanner = new Scanner(System.in);
    private TodoList todoList = new TodoList();

    /**
     * Méthode qui va lancer l'interface en ligne de commande (CLI) avec une
     * boucle infinie qui nous demandera une nouvelle tâche à rajouter jusqu'à
     * ce qu'on ferme le programme
     */
    public void start() {
        while (true) {

            System.out.println("Please enter a thing to do");
            String answer = scanner.nextLine();

            todoList.addTask(answer);
            System.out.println("\033[H\033[2J"); //Truc qui "clear" la console, mais en vrai pas vraiment
            System.out.println("Todo List current state : ");
            todoList.displayList();

        }
        // scanner.close();
    }


    //La même chose mais en utilisant la récursivité : une méthode qui s'appelle elle même
    //Genre là, plutôt que faire un while, on fait que start() rappelle start() à la fin, et ça crée une boucle infinie
    /*
     * public void start() {
     * 
     * System.out.println("Please enter a thing to do");
     * String answer = scanner.nextLine();
     * 
     * todoList.addTask(answer);
     * System.out.println("\033[H\033[2J");
     * System.out.println("Todo List current state : ");
     * todoList.displayList();
     * 
     * start();
     * // scanner.close();
     * }
     */
}
