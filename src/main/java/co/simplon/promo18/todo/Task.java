package co.simplon.promo18.todo;

/**
 * Classe représentant une tâche à faire ainsi que si elle est terminée ou non
 */
public class Task {
    private String label;
    private boolean done = false;

    public Task(String label) {
        this.label = label;
    }

    /**
     * Méthode qui passe le done de true à false ou de false à true
     */
    public void check() {
        this.done = !this.done;

        //revient au même que de faire ça
        // if(this.done) {
        //     this.done = false;
        // } else {
        //     this.done = true;
        // }

    }
    /**
     * Méthode qui va renvoyer l'affichage de la Task actuelle selon son état done
     * @return Le label et une coche vide si la tâche n'est pas terminée, sinon renvoie le label et la coche cochée
     */
    public String display() {
        if(!done) {
            return "☐ "+label;
        }
        return "☒ "+label;


    }

    public boolean isDone() {
        return done;
    }
    
}
