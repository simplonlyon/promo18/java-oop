# Java OOP

Exercice de POO en Java


## Exercices
(Ne pas hésiter à regarder les différents tags qui amènent vers un commit spécifique pour les différentes parties de l'exercices)

### [Todo List](src/main/java/co/simplon/promo18/todo/)

```mermaid
classDiagram
class Task {
    -label: String
    -done: boolean
    +check()
    +display() String
}
class TodoList {
    -tasks: List~Task~
    +addTask(label:String)
    +displayList()
}
Task -- TodoList
```
#### I. La classe Task
1. Créer une classe co.simplon.promo18.javaoop.todo.Task qui aura une propriété label en String et une propriété done en boolean, les deux private
2. Dans cette classe, créer une méthode check() qui va passer le done de true à false ou de false à true
3. Créer également un constructeur qui attendra un label et qui initialisera le done à false 
#### II. La classe TodoList
1. Créer une classe TodoList (dans le même package que Task) qui possédera une propriété tasks de type List<Task> initialisée en ArrayList vide
2. Dans cette classe, faire une méthode addTask(label) qui va attendre un argument de type String et s'en servir pour créer une nouvelle instance de Task et l'ajouter dans la liste tasks
#### III. Affichage de la TodoList et des Task
1. Dans la classe Task, créer une méthode display() qui va renvoyer du String, en l'occurrence on veut ça renvoie une coche vide et le label si done est false et sinon que ça affiche une coche pleine et le label si done est true (exemple : ☐ Faire un truc ou bien ☒ Faire un truc)
2. Dans la classe TodoList, rajouter également une méthode display() qui va faire une boucle sur toutes tasks qu'elle contient et faire un sout de leur méthode display() à elle 
#### IV. L'UI de la TodoList (qui ici sera un cli avec Scanner)
1. Créer une nouvelle classe todo.CliTodoList et dans cette classe, rajouter une propriété de type TodoList initialisée avec une instance de TodoList directement
2. Créer une méthode start() qui ne renverra rien, et à l'intérieur, créer un Scanner qui va nous demander quelle tâche rajouter. On récupère la valeur du nextLine et on s'en sert pour faire un addTask suivi d'un displayList sur la TodoList
3. Faire une boucle dans start() pour que ça nous redemande des nouvelles tâches à jamais. Puis déclencher la méthode start depuis le main 
#### V. Cocher/Décocher nos Task depuis la TodoList
au moins 2 méthodes sont possibles, une qui expose les tasks à l'extérieur et l'autre qui répète un peu les méthodes de task dans todolist. Les deux ont leurs avantages et inconvénient, on va commencer par celle qui expose la liste ici
1. Dans la classe TodoList, rajouter un getTasks() (qu'on peut auto généré via le generate getter/setter) qui va permettre de récupérer la liste des tasks
2. Maintenant qu'on a ça, on peut directement faire un getTasks() sur une TodoList et dessus on peut faire un .get(0).check() par exemple pour cocher/décocher le premier élément de la liste

Pour l'autre manière, voici les étapes :
1. Dans la classe TodoList, rajouter une méthode checkTask(int index) qui va faire un get(index) sur la liste de tasks puis déclencher la méthode check de celle ci
2. Depuis le main ou autre, on appelle la méthode checkTask en lui donnant l'index de ce que l'on souhaite cocher/décocher 